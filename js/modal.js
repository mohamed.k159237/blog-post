const Content = {};
const Title = `<h2 class="modal-title">Organize your digital assets with a new methodology and there.</h2>`;

Content["blog-1"] = `
    ${Title}
    <img style="width:100%;" src="img/blog-1.png"/>
    `;

Content["blog-2"] = `
    ${Title}
    <img style="width:100%;" src="img/blog-2.png"/>
    `;
Content["blog-3"] = `
    ${Title}
    <img style="width:100%;" src="img/blog-3.png"/>
    `;
Content["blog-4"] = `
    ${Title}
    <img style="width:100%;" src="img/blog-4.png"/>
    `;
Content["blog-5"] = `
    ${Title}
    <img style="width:100%;" src="img/blog-5.png"/>
    `;
Content["blog-6"] = `
    ${Title}
    <img style="width:100%;" src="img/blog-6.png"/>
    `;

window.State = {
  content: null,
  isOpen: false,
  openDialog: function (key) {
    State.content = Content[key];
    State.isOpen = true;
    view();
  },
  closeDialog: function () {
    State.isOpen = false;
    view();
  },
};

function view() {
  var modalCls = State.isOpen ? "modal modal-shown" : "modal modal-hidden";
  modal.innerHTML = `
    <div class="${modalCls}">
      <div class="modal-content">
        <div style="text-align:right" onclick="State.closeDialog()">
          <span class="modal-close">✕</span>
        </div>
        <div style="text-align:center">${State.content}</div>
      </div>
    </div>
  `;
}

triggerModal.addEventListener("click", function (e) {
  var modal = e.target.getAttribute("modal");
  modal && State.openDialog(modal);
});
