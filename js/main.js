/******Email-Validation-Errors******/
function showError(conteinerParentNode, errorMessage) {
  conteinerParentNode.className = "error";
  const error = document.createElement("p");
  error.className = "error-message";
  error.innerHTML = errorMessage;
  conteinerParentNode.appendChild(error);
}

function resetError(conteinerParentNode) {
  conteinerParentNode.className = "";
  if (conteinerParentNode.lastChild.className == "error-message") {
    conteinerParentNode.removeChild(conteinerParentNode.lastChild);
  }
}

function validate(form) {
  const elems = form.elements;
  const regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  resetError(elems.email.parentNode);
  if (!elems.email.value) {
    showError(elems.email.parentNode, " Please enter your Email. ⓘ");
    return false;
  } else if (!regEmail.test(elems.email.value) == true) {
    showError(elems.email.parentNode, " Please enter a valid Email.ⓘ");
    return false;
  } else {
    error.innerHTML = "";
  }

  return true;
}
